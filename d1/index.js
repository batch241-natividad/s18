//alert("Hi!");
/*
	You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function

*/

/*
	Syntax:

		function functionName(parameter) {
				code block
		}
		functionName(argument)

*/

//name is called parameter
//"parameter" acts as a named variable/container that exists only inside of a function
//it is used to store information that is provided to a function when it is called/invoked

let name = "John";

function printName(name) {

	console.log("My Name is " + name);
}


//"Juan" and "Miah", the information/data provided directly into the function, it is called an argument
printName("Juana");
printName("Miah");

//Variables can also be passed as an ARGUMENT
let userName = "Elaine";

printName(userName);

printName(name);
printName(); // function calling without arguments will result to undefined parameters.

//
function greeting() {
	console.log("Hello, User!");
}

greeting("Eric");

//Using Multiple Parameters
//Multiple "arguments" will corresponds to the number of "parameters" declared in a function in succeeding order.
//Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.
//The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.


function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " "+ lastName)
}

createFullName("Erven", "Joshua", "Cabral");
//"Erven" will be stored in the parameter "firstName"
//"Joshua" will be stored in the parameter "middleName"
//"Cabral" will be stored in the parameter "lastName"


//In JS, providing more/less arguments than the expected parameters will not return an error
// In other programming languages, this will return an error stating that "the expected number of argument do not match the number of parameters".

createFullName("Eric", "Andales");
createFullName("Roland", "John", "Doroteo", "Jane");

function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is " + remainder);
	let isDivisibleBy8 = (remainder === 0);
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

/*
	Mini-Activity:
		1. Create a function which is able to  receive data as an argument.
			-This function should be able to receive the name of your favorite superhero.
			-Display the name of your favorite superhero in the console.

	Kindly send the ss in our gc.



*/

function printSuperHero(superHero){

	console.log("My favorite superhero is " + superHero)

}

printSuperHero("Batman");

//Return Statement

/*
	The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.
*/	

function returnFullName(firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName

	console.log("Can we print this message?");

}

// Whatever value that is returned from the "returnFullName" function can be stored in a variable.

let completeName = returnFullName("John", "Doe", "Smith");
console.log(completeName);

function returnAddress(city, country) {
	let fullAddress = city + ", " + country;
	return fullAddress
}


let myAddress = returnAddress("Sta.Mesa, Manila", "Philippines");
console.log(myAddress);

//Consider the ff code.
/*

Mini-activity:
		1.Debug our code. So that the function will be able to return a value and save it in a variable.

*/
function printPlayerInformation(userName, level, job){

	console.log("Username: " + userName);
	console.log("Level: " + level);
	console.log("Job: " + job);

	return  userName + level + job

}






let user1 = printPlayerInformation("cardo123", "999", "Immortal");
console.log(user1);//defined

//returns undefined because printPlyaerInformation returns nothing. It only loghs the message in the console 
//You cannot save any value from printPlayerInfo() because it does not RETURN anything.

